# MSMC2 analysis (Population history)

## Samples selection
Select 8 strains in each population by PCA.
MSMC2 support maximum 8 haploid in one population

|Strains|Population|Strains|Population|Strains|Population|
|:-----:|:--------:|:-----:|:--------:|:-----:|:--------:|
|171169|POP1|170827|POP2|171658|POP3|
|171668|POP1|140007|POP2|172122|POP3|
|171322|POP1|160232|POP2|171646|POP3|
|170888|POP1|140022|POP2|172123|POP3|
|171287|POP1|180459|POP2|171651|POP3|
|171301|POP1|170960|POP2|171639|POP3|
|170789|POP1|160242|POP2|171482|POP3|
|171278|POP1|140014|POP2|172118|POP3|

## Generate vcf of these strains

GATK was used for SNP calling with gvcf files generated previously
```bash
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' CombineGVCFs -R 180197_pilon4_nomt_masked.fas --variant msmc_vcf/msmc_POP1_vcf.list -O msmc_vcf/msmc_pop1_combined.g.vcf
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' GenotypeGVCFs -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/msmc_pop1_combined.g.vcf -O msmc_vcf/msmc_pop1.vcf
```
select SNP
```bash
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' SelectVariants -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/msmc_pop1.vcf -O msmc_vcf/msmc_SNP_pop1.vcf --select-type-to-include SNP
```

## vcf filter
filter vcf by vcfR
```bash
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' VariantFiltration -V msmc_vcf/msmc_SNP_pop1.vcf --filter-expression 'QD < 2.0 || MQ < 40.0 || FS > 60.0 || SOR > 3.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0' --filter-name 'my_filter' -O msmc_vcf/msmc_SNP_pop1_marked.vcf
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' SelectVariants -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/msmc_SNP_pop1_marked.vcf -O msmc_vcf/msmc_SNP_pop1_filter_gatk.vcf -select 'vc.isNotFiltered()'
cd /home/zhanghao/asiaticum/call_snp_220409/msmc_vcf
conda activate R4.1
# filter by dp and missing
Rscript vcf_filter_outgroup_msmc.R
```
Content of vcf_filter_outgroup_msmc.R:
```R
library(vcfR)
library(tidyverse)

setwd("/home/zhanghao/asiaticum/call_snp_220409/msmc_vcf")
vcf <- read.vcfR("msmc_SNP_pop3_filter_gatk.vcf")
dp <- extract.gt(vcf, element = "DP", as.numeric = TRUE)

# filter dp at 95% 
sums <- apply(dp, MARGIN=2, quantile, probs=c(0.05, 0.95), na.rm=TRUE)
dp2 <- sweep(dp, MARGIN=2, FUN = "-", sums[1,])
dp[dp2 < 0] <- NA
dp2 <- sweep(dp, MARGIN=2, FUN = "-", sums[2,])
dp[dp2 > 0] <- NA
dp[dp < 4] <- NA
myMiss <- apply(dp, MARGIN = 1, function(x){sum(is.na(x))})
myMiss <- myMiss / ncol(dp)
vcf_filter <- vcf[myMiss == 0, ]
#write.vcf(vcf_filter, 'msmc_pop1_vcfR.vcf.gz')

gt <- extract.gt(vcf_filter, element = "GT", as.numeric = TRUE)
vcf_filter2 <- vcf_filter[complete.cases(gt), ]

vcf_filter_delete <- vcf_filter2[!grepl("\\*", vcf_filter2@fix[,5]),]
write.vcf(vcf_filter_delete, 'msmc_pop3_vcfR_noasterisk.vcf.gz')
```

## Generate masked file
1. use bedtools calculate coverage of each site in each strain
2. coverage higher than quantile 99.9% or lower than 6 were masked sites
3. combine the sites in all 8 strains in one POP

```bash
./prepare_mask_file.py
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import subprocess
from collections import defaultdict

F = defaultdict(list)
G = defaultdict(list)
with open("msmc_strains.txt", "r") as f1:
    for line in f1:
        line = line.strip().split()
        G[line[1]].append(line[0])
        subprocess.check_call("bedtools genomecov -ibam /home/zhanghao/asiaticum/call_snp_220409/output/bam/{0}.sort.bam -d -split > mask_bed/{0}.genomecov.bed".format(line[0]), shell= True)
        data = pd.read_csv("mask_bed/{0}.genomecov.bed".format(line[0]), sep="\t", header=None)
        top = data[2].quantile(0.999)
        sel = data[(data[2] < 6) | (data[2] > top)]
        with open("mask_bed/" + line[1] + "_" + line[0] + ".mask", "w") as f2:
            for index,row in sel.iloc[:, 0:2].iterrows():
                F[line[0]].append(row[0] + "\t" + str(row[1]))
                f2.write(row[0] + "\t" + str(row[1]) + "\n")

for k,v in G.items():
    L = []
    for i in v:
        if len(L) > 0:
            L = list(set(L).union(F[i]))
        else:
            L = F[i]
    with open("mask_bed/{0}.mask".format(k), "w") as f3:
        for x in L:
            f3.write(x + "\n")
```

## Generate MSMC2 input files

 prepare_msmc_input2.py vcf_without_filter filtered_vcf mask_file output_prefix
 produce seperate input file for each chromosome 
```bash
./prepare_msmc_input.py msmc_pop1.vcf msmc_pop1_vcfR_noasterisk.vcf.gz pop1.mask msmc_pop1
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# original_vcf, filtered_vcf, output_prefix
import sys, gzip
from collections import defaultdict

F = defaultdict(list)
with open(sys.argv[1], 'r') as f1:
    for line in f1:
        if not line.startswith("#"):
            line = line.strip().split()
            F[line[0]].append(line[1])

with gzip.open(sys.argv[2], "r") as f2:
    L = {}
    for line in f2:
        line = line.decode()
        if not line.startswith("#"):
            line = line.strip().split()
            if not line[0] in L:
                L[line[0]] = []
                n = 0
                no_snp = 0
            nucl = [x for x in line[4].split(",")]
            nucl.insert(0, line[3])
            seq = []
            for i in line[9:]:
                gt = i.split(":")[0]
                seq.append(nucl[int(gt)])
            if len(set(seq)) == 1:
                no_snp = no_snp + 1
            else:
                len_gap = int(line[1]) - n
                if n == 0:
                    mask = F[line[0]].index(line[1])
                else:
                    mask = F[line[0]].index(line[1]) - F[line[0]].index(str(n)) - 1
                len_homo = len_gap - mask - no_snp
                L[line[0]].append(line[0].replace("Fasi_180197_V1_chr0", "") + "\t" + line[1] + "\t" + str(len_homo) + "\t" + "".join(seq))
                n = int(line[1])
                no_snp = 0

for k,v in L.items():
    chrm = k.replace("Fasi_180197_V1", "")
    with open(sys.argv[3] + chrm + ".input", "w") as f:
        for i in v:
            f.write(i + "\n")
```
## rum msmc2
```bash
msmc2_linux64bit -t 50 -o pop1_.msmc2 -I 0,1,2,3,4,5,6,7 POP1_chr01.input POP1_chr02.input POP1_chr03.input POP1_chr04.input
```

# The pipeline for MSMC2
## From gvcf to msmc2 result
Ten repeats for each population

```bash
./batch_msmc2_ne.py
```

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import defaultdict
from multiprocessing import Pool
import random, subprocess
import pandas as pd

def run_msmc2(input):
    prefix = input[0]
    sel = input[1]
    with open(prefix + "_strains.list", "w") as f2:
        for i in sel:
            # path of gvcfs for CombineGVCFs 
            f2.write("output/gvcf/" + i + ".g.vcf.gz" + "\n") 
    subprocess.check_call("docker run --rm -u $(id -u):$(id -u) -v /home/zhanghao/asiaticum/call_snp_220409:/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' CombineGVCFs -R 180197_pilon4_nomt_masked.fas --variant msmc_vcf/{0}_strains.list -O msmc_vcf/{0}_combined.g.vcf".format(prefix), shell = True)
    subprocess.check_call("docker run --rm -u $(id -u):$(id -u) -v /home/zhanghao/asiaticum/call_snp_220409:/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' GenotypeGVCFs -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/{0}_combined.g.vcf -O msmc_vcf/{0}.vcf".format(prefix), shell = True)
    subprocess.check_call("docker run --rm -u $(id -u):$(id -u) -v /home/zhanghao/asiaticum/call_snp_220409:/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' SelectVariants -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/{0}.vcf -O msmc_vcf/{0}_SNP.vcf --select-type-to-include SNP".format(prefix), shell = True)
    subprocess.check_call("docker run --rm -u $(id -u):$(id -u) -v /home/zhanghao/asiaticum/call_snp_220409:/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' VariantFiltration -V msmc_vcf/{0}_SNP.vcf --filter-expression 'QD < 2.0 || MQ < 40.0 || FS > 60.0 || SOR > 3.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0' --filter-name 'my_filter' -O msmc_vcf/{0}_SNP_marked.vcf".format(prefix), shell = True)
    subprocess.check_call("docker run --rm -u $(id -u):$(id -u) -v /home/zhanghao/asiaticum/call_snp_220409:/gatk_wd -w /gatk_wd broadinstitute/gatk:latest gatk --java-options '-Xmx500g' SelectVariants -R 180197_pilon4_nomt_masked.fas -V msmc_vcf/{0}_SNP_marked.vcf -O msmc_vcf/{0}_SNP_filter_gatk.vcf -select 'vc.isNotFiltered()'".format(prefix), shell = True)
    subprocess.check_call("Rscript vcf_filter_outgroup_msmc.R {0}_SNP_filter_gatk.vcf {0}_vcfR_noasterisk.vcf.gz".format(prefix), shell = True)
    # prepare mask file
    df = pd.DataFrame()
    for i in sel:
        subprocess.check_call("bedtools genomecov -ibam /home/zhanghao/asiaticum/call_snp_220409/output/bam/{0}.sort.bam -d -split > mask_bed/{0}.genomecov.bed".format(i), shell= True)
        data = pd.read_csv("mask_bed/{0}.genomecov.bed".format(i), sep="\t", header=None)
        top = data[2].quantile(0.999)
        mask = data[(data[2] < 6) | (data[2] > top)]
        mask = mask.iloc[:,0:2]
        df = pd.concat([df, mask])
    df = df.drop_duplicates()
    with open("{0}.mask".format(prefix), "w") as f3:
        for index,row in df.iterrows():
            f3.write(row[0] + "\t" + str(row[1]) + "\n")
    subprocess.check_call("./prepare_msmc_input2.py {0}.vcf {0}_vcfR_noasterisk.vcf.gz {0}.mask {0}".format(prefix), shell = True)
    subprocess.check_call("msmc2_linux64bit -t 4 -o {0}.msmc2 -I 0,1,2,3,4,5,6,7 {0}_chr01.input {0}_chr02.input {0}_chr03.input {0}_chr04.input".format(prefix), shell = True)

if __name__ == '__main__':
    F = defaultdict(list)
    G = defaultdict(list)
    L = []
    with open("fa_pops.list", "r") as f1:
        for line in f1:
            line = line.strip().split()
            F[line[1]].append(line[0])
    for k,v in F.items():
        for n in range(10):
            # select 8 samples randomly from this population 
            sel = random.sample(v, 8)
            prefix = k + "_rep" + str(n)
            L.append([prefix, sel])
    pool = Pool(30)                         # Create a multiprocessing Pool
    pool.map(run_msmc2, L)
```

## Plot the MSMC2 results

prefix.final.txt is the result for ploting

```R
library(tidyverse)
library(RColorBrewer)
library(patchwork)

mu <- 0.58e-8
gen <- 1
df <- data.frame()

for (i in 1:3){
  for (j in 0:9){
    data <- read.table(paste0("POP", i, "_rep", j, ".msmc2.final.txt"), header = T) %>%
      mutate(left_time_boundary = left_time_boundary/mu*gen) %>% 
      mutate(lambda = (1/lambda)/(2*mu)) %>% 
      mutate(population = paste0("POP", i, "_", j)) %>%
      mutate(pop = paste0("POP", i))
    df <- rbind(df, data)
  }
}
  
for (i in 1:3){
  for (j in 0:9){
    data <- read.table(paste0("POP", i, "_rep", j, "_2nd.msmc2.final.txt"), header = T) %>%
      mutate(left_time_boundary = left_time_boundary/mu*gen) %>% 
      mutate(lambda = (1/lambda)/(2*mu)) %>% 
      mutate(population = paste0("POP", i, "_", j, "_2nd")) %>%
      mutate(pop = paste0("POP", i))
    df <- rbind(df, data)
  }
}
 
pop1 <- c("POP1_0", "POP1_8_2nd", "POP1_0_2nd", "POP1_6", "POP1_9_2nd") #"POP1_4"
pop2 <- c("POP2_0", "POP2_2", "POP2_4", "POP2_1_2nd", "POP2_2_2nd")
pop3 <- c("POP3_3_2nd", "POP3_1", "POP3_3_2nd", "POP3_5", "POP3_6_2nd")

pop_sel <- c(pop1, pop2, pop3)
col1 <- colorRampPalette(c('#D45B65','#DE0417'))(5)
col2 <- colorRampPalette(c('#9AE0FA','#4151F3'))(5)
col3 <- colorRampPalette(c('#A5FA9A','#0C7B04'))(5)
col <- c(col1, col2, col3)

p1 <- ggplot() + 
  geom_step(data = df[df$population %in% pop_sel,], aes(x = left_time_boundary, y = lambda, color = population), show.legend = F) +
  scale_color_manual(values = col) +
  scale_x_log10() + 
  scale_y_log10() + 
  xlab("") + 
  ylab("effective population size")

p2 <- ggplot() + 
  geom_step(data = df[df$population %in% pop_sel,], aes(x = left_time_boundary, y = lambda, color = population), show.legend = F) +
  scale_color_manual(values = col) +
  scale_x_log10() + 
  scale_y_log10() + 
  xlab("year ago") + 
  ylab("effective population size") +
  facet_wrap(~pop)

p1/p2
```
