# The Minigraph-Cactus Pangenome Pipeline

## Prepare files

Apply naming convention where non-reference haploid samples have ".0" suffixes.
```
mkdir cactus

ls | grep ordered_soft > cactus/245_genomes.txt

cd cactus

sed -i '/PH/d' 245_genomes.txt

perl -pe 's/(\S+)_ordered_soft\.fas/$1\.0\t\/work\/$1_ordered_soft\.fas/g' 245_genomes.txt > 245_genomes_path.txt

perl -pe 's/180197\.0/180197/g' 245_genomes_path.txt > genomes_245_path.txt

cd ..
```

## Make the SV graph with minigraph
```
screen -L docker run --rm -u $(id -u):$(id -u) -v $(pwd):/work -w /work quay.io/comparative-genomics-toolkit/cactus:v2.2.3 cactus-minigraph cactus/jobstore cactus/genomes_245_path.txt cactus/fa_245.gfa.gz --mapCores 100 --reference 180197
```

## Make the assembly-to-graph alignments with minigraph
```
screen -L docker run --rm -u $(id -u):$(id -u) -v $(pwd):/work -w /work quay.io/comparative-genomics-toolkit/cactus:v2.2.3 cactus-graphmap cactus/jobstore cactus/genomes_245_path.txt cactus/fa_245.gfa.gz cactus/fa_245.paf --mapCores 100 --reference 180197 --outputFasta cactus/fa_245.gfa.fa.gz
```

## Create the Cactus base alignment and "raw" pangenome graph
```
screen -L docker run --rm -u $(id -u):$(id -u) -v $(pwd):/work -w /work quay.io/comparative-genomics-toolkit/cactus:v2.2.3 cactus-align cactus/jobstore cactus/genomes_245_path.txt cactus/fa_245.paf cactus/fa_245.hal --pangenome --outVG --reference 180197 
```

## Create and index the final pangenome graph and produce a VCF
```
# --giraffe will crash
screen -L docker run --rm -u $(id -u):$(id -u) -v $(pwd):/work -w /work quay.io/comparative-genomics-toolkit/cactus:v2.2.3 cactus-graphmap-join cactus/jobstore --vg cactus/fa_245.vg --outDir cactus --outName fa_245_pan --reference 180197 --vcf --gfaffix --wlineSep "." --clipLength 10000 --clipNonMinigraph
```

## filter the vcf file
allels frequency > 0.05 and indivials > 200
select indels, output length of ref and alt for further filtering large sv 
```
./screen_sv.py
```
@import "script/screen_sv.py"