# Install braker2 pipline
## Install Augustus 3.4
### Set conda env
```sh
conda create -n braker21 python=3.7
conda activate braker2
```
### Update gcc to v4.9:
gcc version >= 7 changed to 3 packages in anaconda, gcc, gxx and gfortran  
Higher version gcc is incompatible with bamtools 2.5.1 (not sure ......), 2.5.2 support GCC 11
```sh
conda install -c serge-sans-paille gcc_49
cd anaconda2/envs/braker2/bin
ln -s gcc-4.9 gcc
ln -s g++-4.9 g++
ln -s c++-4.9 c++
```
### Update cmake to 3.19.6
```sh
conda install cmake
cmake --version
cmake version 3.19.6
CMake suite maintained and supported by Kitware (kitware.com/cmake).
```
### Bamtools 2.5.1
```sh
cd ~/software/bamtools-2.5.1
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/home/bac/software/bamtools-2.5.1 ..
make
make install
# add to $PATH (.zshrc)
export BAMTOOLS_PATH=$HOME/software/bamtools-2.5.1/bin
```
### Samtools, htslib, bcftools and tabix
1. Install all the 4 in one folder, remove version in the folder name to make them the same with Makefile
2. Change env with gcc >7 to compile samtools, htslib, bcftools
3. Change back to env with gcc4.9 to compile tabix
```sh
mkdir augustus_dep # outside of Augustus
cd augustus_dep
conda deactivate # change to base env with gcc7 

# htslib
wget https://github.com/samtools/htslib/releases/download/1.11/htslib-1.11.tar.bz2
mv htslib-1.11 htslib
cd htslib
autoheader
autoconf
./configure --prefix=/home/bac/software/augustus_dep/htslib
make
make install
cd ..

# samtools
wget https://github.com/samtools/samtools/releases/download/1.11/samtools-1.11.tar.bz2
mv samtools-1.11 samtools
cd samtools
autoheader
autoconf
./configure --prefix=/home/bac/software/augustus_dep/samtools
make
make install
cd ..

# bcftools
wget https://github.com/samtools/bcftools/releases/download/1.11/bcftools-1.11.tar.bz2
mv bcftools-1.11 bcftools
cd bcftools
autoheader
autoconf
./configure --prefix=/home/bac/software/augustus_dep/bcftools
make
make install
cd ..
```

```sh
conda activate braker2 # env with gcc4.9

# tabix
git clone https://github.com/samtools/tabix.git
cd tabix
make
cd ..
```
### Modify Makefile in bam2hints
```sh
git clone https://github.com/Gaius-Augustus/Augustus.git
cd Augustus/auxprogs/bam2hints/

#add
BAMTOOLS = /home/bac/software/bamtools-2.5.1
# replace
INCLUDES = -I/usr/include/bamtools
# by
INCLUDES = -I$(BAMTOOLS)/include/bamtools

# replace
LIBS = -lbamtools -lz
# by
LIBS = $(BAMTOOLS)/lib/libbamtools.a -lz
```
### Modify Makefile in filterBam
```sh
cd Augustus/auxprogs/filterBam/src

# replace
BAMTOOLS = /usr/include/bamtools
# by
BAMTOOLS = /home/bac/software/bamtools-2.5.1
# replace
INCLUDES = -I$(BAMTOOLS) -Iheaders -I./bamtools
# by
INCLUDES = -I$(BAMTOOLS)/include/bamtools -Iheaders -I./bamtools

# replace
LIBS = -lbamtools -lz
# by
LIBS = $(BAMTOOLS)/lib/libbamtools.a -lz
```
### Modify Makefile in bam2wig
```sh
cd Augustus/auxprogs/bam2wig

# add
TOOLDIR = /home/bac/software/augustus_dep
```
### Compile Augustus 3.4 
```sh
make
# add tp $PATH (.zshrc)
export PATH="/home/bac/software/Augustus/bin:/home/bac/software/Augustus/scripts:$PATH"
export AUGUSTUS_CONFIG_PATH=/home/bac/software/Augustus/config
# /usr/lib/x86_64-linux-gnu/libstdc++.so.6 version is low, add conda lib in PATH
export LD_LIBRARY_PATH=/home/bac/anaconda2/envs/braker2/lib:$LD_LIBRARY_PATH
```
## Perl pipeline dependencies
```sh
conda install -c anaconda perl
conda install -c bioconda perl-app-cpanminus
conda install -c bioconda perl-hash-merge
conda install -c bioconda perl-parallel-forkmanager
conda install -c bioconda perl-scalar-util-numeric
conda install -c bioconda perl-yaml
conda install -c bioconda perl-class-data-inheritable
conda install -c bioconda perl-exception-class
conda install -c bioconda perl-test-pod
conda install -c anaconda biopython
conda install -c bioconda perl-file-homedir
conda install -c bioconda perl-file-which # skip if you are not comparing to reference annotation
conda install -c bioconda perl-mce
conda install -c bioconda perl-threaded
conda install -c bioconda perl-list-util
conda install -c bioconda perl-list-moreutils
conda install -c bioconda perl-math-utils
conda install -c bioconda cdbtools
```
## ProtHint 2.6.0 & GeneMark-ES/ET/EP 4.65_lic (Diamond, spaln, NCBI BLAST+ 2.2.31+)
```sh
git clone https://github.com/gatech-genemark/ProtHint.git
cd ProtHint/dependencies/GeneMarkES
wget http://topaz.gatech.edu/GeneMark/tmp/GMtool_h9kL7/gmes_linux_64.tar.gz
tar xzvf gmes_linux_64.tar.gz
mv gmes_linux_64/* .
rm -rf gmes_linux_64
cd ~
mv gm_key_64 .gm_key
cd /software/ProtHint/dependencies/GeneMarkES
sed -i 's/usr\/bin\/perl/usr\/bin\/env perl/g' *.pl # use perl in env
```
```sh
./check_install.bash # check installation of GeneMark-ES/ET/EP
# output
Checking GeneMark-ES installation
Checking Perl setup
All required Perl modules were found
Checking GeneMark.hmm setup
GeneMark.hmm was found
GeneMark.hmm is set
GeneMark.hmm is executable
Performing GeneMark.hmm test run
All required components for GeneMark-ES were found
```
```sh
# add to $PATH (.zshrc)
export DIAMOND_PATH=/home/bac/software/ProtHint/dependencies
export BLAST_PATH=/home/bac/software/ncbi-blast-2.2.31+/bin
export PROTHINT_PATH=/home/bac/software/ProtHint/bin
export GENEMARK_PATH=/home/bac/software/ProtHint/dependencies/GeneMarkES
```
## GenomeThreader 1.7.1
v1.7.3 is incompatible
```sh
wget https://genomethreader.org/distributions/gth-1.7.1-Linux_x86_64-64bit.tar.gz
tar xzvf gth-1.7.1-Linux_x86_64-64bit.tar.gz
# add to $PATH (.zshrc)
export BSSMDIR=/home/bac/software/gth-1.7.1-Linux_x86_64-64bit/bin/bssm
export ALIGNMENT_TOOL_PATH=/home/bac/software/gth-1.7.1-Linux_x86_64-64bit/bin
```
## Samtools 1.7
```sh
wget https://phoenixnap.dl.sourceforge.net/project/samtools/samtools/1.7/samtools-1.7.tar.bz2
tar xjvf samtools-1.7.tar.bz2
autoheader
autoconf
./configure --prefix=/home/bac/software/samtools-1.7
make
make install
# add to $PATH (.zshrc)
export SAMTOOLS_PATH=/home/bac/software/samtools-1.7
```
## braker2
```sh
git clone https://github.com/Gaius-Augustus/BRAKER.git
# add to $PATH (.zshrc)
export PATH="/home/bac/software/Augustus/scripts:$PATH"
```
### Test braker2
```sh
cd ~/software/BRAKER/example
wget http://topaz.gatech.edu/GeneMark/Braker/RNAseq.bam
cd test
sh test3.sh
```
