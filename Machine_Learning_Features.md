## Machine Learning Feature Descriptors for Nucleotide Sequence

### 1.	Kmer
### 2.	RCkmer
&emsp;&emsp;将反向互补的kmer合并（实现：每个kmer反向互补后rckmer，sorted[kmer, rckmer][0]）
### 3.	Nucleic Acid Composition (NAC)
&emsp;&emsp;只有4维，统计每种碱基数量
### 4.	Di-Nucleotide Composition (DNC)
&emsp;&emsp;相当于kmer k=2
### 5.	Tri-Nucleotide Composition (TNC)
&emsp;&emsp;相当于kmer k=3
### 6.	Enhanced Nucleic Acid Composition (ENAC)
&emsp;&emsp;设定滑窗k，统计每个滑窗内4种碱基数量，序列长度L，数据维度(L-k+1)*4，所有序列长度需相等
### 7.	Binary
&emsp;&emsp;每个碱基用4维向量表示，例如：A is encoded by (1000), C is encoded by (0100), G is encoded by (0010) and T(U) is encoded by (0001)。One-hot编码似乎一样。所有序列长度需相等
### 8.	Composition of k-spaced Nucleic Acid Pairs (CKSNAP)
&emsp;&emsp;设定k值，间隔为0-k的二核苷酸统计，例如k=1，02,13,24…..位置的核苷酸组合一起统计，除以总数量序列长度L-k+1，数据维度16*k
### 9.	Nucleotide Chemical Property (NCP)
&emsp;&emsp;根据核酸（RNA）三类化学性质将碱基三维向量化，所有序列长度需相等。
### 10.	Accumulated Nucleotide Frequency (ANF)
&emsp;&emsp;计算每个核苷酸的累计频率并结合NCP，每个核苷酸成为一个4维向量，同时考虑了化学性质和长度信息，所有序列长度需相等。举例：“UCGUUCAUGG”. The density of ‘U’ is 1 (1/1), 0.5 (2/4), 0.6 (3/5), 0.5 (4/8) at positions 1, 4, 5, and 8, respectively. The density of ‘C’ is 0.5 (1/2), 0.33 (2/6) at positions 2 and 6, respectively. The density of ‘G’ is 0.33 (1/3), 0.22 (2/9), 0.3 (3/10) at positions 3, 9, and 10, respectively. The density of ‘A’ is 0.14 (1/7) at position 7. So it can be represented by {(0, 0, 1, 1), (0, 1, 0, 0.5), (1, 0, 0,0.33), (0, 0, 1, 0.5), (0, 0, 1, 0.6), (0, 1, 0, 0.33), (1, 1, 1, 0.14), (0, 0, 1, 0.5), (1, 0, 0, 0.22), (1, 0, 0,0.3)}.
### 11.	Position-specific trinucleotide propensity based on single-strand (PSTNPss)
&emsp;&emsp;要求序列等长，设置training和testing标签，内部二分类或多分类1为positive，其他为negative，仅输出testing个体的特征数据，L-2维。先把training序列生成64行（$4^{3}$）L-2列矩阵，每一列代表3核苷酸kmer滑窗位置，每行代表一个kmer类型（$4^{3}$种），按照3mer位置和类型计数，填写矩阵，positive和negative分为2个矩阵。遍历testing序列3mer，在p和n两个矩阵中找到行列对应的数据，对应的数据如果大于0就减1（相当于从training中减去testing个数） po_number = p[x][y]-1或ne_number = n[x][y]-1，training的p或n的总数量也同时减1， 计算此位置3mer的数据 po_number/p_num - ne_number/n_num
### 12.	Position-specific trinucleotide propensity based on double-strand (PSTNPds)
&emsp;&emsp;与PSTNPss类似，合并反向互补数据
### 13.	Electron-ion interaction pseudopotentials (EIIP)
&emsp;&emsp;用EIIP值代替碱基 (A: 0.1260, C: 0.1340, G: 0.0806, T:0.1335)，所有序列长度需相等。
### 14.	Electron-ion interaction pseudopotentials of trinucleotide (PseEIIP)
&emsp;&emsp;kmer（3）结合EIIP，3mer频率\*3碱基EIIP之和，64维
### 15.	Autocorrelation (ACC)
&emsp;&emsp;两种方法AC和CC，结合二核苷酸和三核苷酸  
&emsp;&emsp;Di: 每个2mer 148种（默认6）赋值方法，tri每个3mer 12种（默认2）赋值方法  
&emsp;&emsp;AC：meanValue，每个方法kmer value相加/kmer数；根据设定间隔lag，每个方法求  
&emsp;&emsp;&ensp;&ensp;(kmerValue – meanValue)\*(kmerValue(间隔) – meanValue)的和，  
&emsp;&emsp;&ensp;&ensp;再求平均值，最终每个方法有lag个值，  
&emsp;&emsp;&ensp;&ensp;数据维度为：n\*lag（n默认di 6，tri 2)  
&emsp;&emsp;CC：与AC相似，不同在于，2个方法排列组合，每个组合求  
&emsp;&emsp;&ensp;&ensp;(kmerValue[方法1] – meanValue1)*(kmerValue[方法2] (间隔) – meanValue2) 的和，  
&emsp;&emsp;&ensp;&ensp;再求平均值，最终每个组合有lag个值，  
&emsp;&emsp;&ensp;&ensp;数据维度为：n(n-1) *lag（n默认di 6，tri 2）  
&emsp;&emsp;ACC：AC与CC组合，数据维度为：n\*lag + n(n-1) \*lag = n2\*lag  
&emsp;&emsp;dinucleotide-based auto covariance (DAC)  
&emsp;&emsp;dinucleotide-based cross covariance (DCC)  
&emsp;&emsp;dinucleotide-based auto-cross covariance (DACC)  
&emsp;&emsp;trinucleotide-based auto covariance (TAC)  
&emsp;&emsp;trinucleotide-based cross covariance (TCC)  
&emsp;&emsp;trinucleotide-based auto-cross covariance (TACC)  
### 6.	Pseudo Nucleic Acid Composition (PseNAC)
&emsp;&emsp;PseDNC, PseKNC, PCPseDNC, PCPseTNC, SCPseDNC, SCPseTNC
