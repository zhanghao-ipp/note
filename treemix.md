# Treemix analysis

## Modify vcf file 

produce fake diploid
```
./fake_diploid.py fa_com_snp_filtered_outgroup_gatk_vcfR_noasterisk_maxmissing1.recode.vcf  fa_245_diploid.vcf
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import argv

def parse_fake(original_file, fake_diploid_file):
    with open(original_file, 'r') as f1, open(fake_diploid_file, 'w') as f2:
        for line in f1:
            if line.startswith('#'):
                f2.write(line)
            else:
                line = line.strip().split()
                for i in range(9, len(line)):
                    ind = line[i].split(":")
                    ind[0] = ind[0] + "/" + ind[0]
                    line[i] = ":".join(ind)
                f2.write("\t".join(line) + "\n")

if __name__ == '__main__':
    original_file = argv[1]
    fake_diploid_file = argv[2]
    faked_file = parse_fake(original_file, fake_diploid_file)
```
add unique id to each SNP
```
./add_vcf_id.py
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#import gzip

n = 1
#with gzip.open("fa_245_PH1_diploid.vcf.gz", "r") as f1, open("fa_245_PH1_diploid_id.vcf", "w") as f2:
with open("fa_245_diploid.vcf", "r") as f1, open("fa_245_diploid_id.vcf", "w") as f2:
    for line in f1:
#        line = line.decode()
        if not line.startswith("#"):
            line = line.strip().split()
            line[2] = str(n)
            f2.write("\t".join(line) + "\n")
            n = n + 1
        else:
            f2.write(line)
```

## Prepare input files 
LD filter
```
plink --vcf fa_245_diploid_id.vcf --indep-pairwise 50 10 0.1 -out fa_245_LD --allow-extra-chr --make-bed
plink --bfile fa_245_LD --extract fa_245_LD.prune.in --out fa_245_LD_prune --allow-extra-chr --make-bed
```
Select samples, remove the only one mix isolate
```
plink --bfile fa_245_LD_prune --out fa_244_LD_prune --keep fa_244.list --make-bed --allow-extra-chr
```
Calculate frequency
```
plink --bfile fa_244_LD_prune --freq --missing --within fa_244.clust --allow-extra-chr --out fa_244_freq
gzip fa_244_freq.frq.strat
```

## Convert to TreeMix information
plink2treemix.py script from: https://bitbucket.org/nygcresearch/treemix/downloads/, python2 script
```
python2 plink2treemix.py fa_244_freq.frq.strat.gz treemix.frq.strat.gz
```

## Run Treemix
```
mkdir treemix_result
./run_treemix.py
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess, random
from multiprocessing import Pool

def treemix(m_rep):
    command = "docker run --rm -u $(id -u):$(id -u) -v $(pwd):/root -w /root calkan/treemix:1.13 treemix -i treemix.frq.strat.gz -bootstrap -seed {0} -k 50 -m {1} -o treemix_result/outstemM{1}_rep{2} -root FG -noss"
    subprocess.call(command.format(random.randint(0,10000), m_rep[0], m_rep[1]), shell=True)

if __name__ == '__main__':
    L = []
    for i in range(0,10):
        for j in range(0,100):
            L.append([i, j])
    pool = Pool(100)
    pool.map(treemix, L)
```

## Estimate the number of migrations with R package OptM
All .llik output files for all the runs in a directory: treemix_result
```R
library(OptM)

fa_245.optm <- optM("call_snp_220409/treemix/treemix_result")
plot_optM(fa_245.optm, method = "Evanno", pdf = "call_snp_220409/treemix/plot_optm.pdf")
```
## Plot Treemix trees
plotting_funcs.R script from: https://bitbucket.org/nygcresearch/treemix/downloads/
```R
source("plotting_funcs.R")
#library(R.utils)

for (j in 0:99){
    pdf(paste("M1_plot/","treePlotM1","rep",j,".pdf",sep=""))
    plot_tree(paste("treemix_result/outstemM1","_rep",j, sep = ""))
    dev.off()
}

```