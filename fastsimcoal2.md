# simulate complex evolutionary models by fastsimcoal2

## 1. Observed SFS (site frequency spectrum) by easySFS
The observed SFS can be a derived SFS (i.e. also known as DAF or an unfolded SFS) if the ancestral state is unknown or a minor allele frequency SFS (i.e. MAF or folded SFS) 
We used unfolded SFS here. fa_pops.txt has two columns, isolates and their populations.
```bash
~/software/easySFS/easySFS.py -i three_groups_vcfR_noasterisk_new.vcf.gz -p fa_pops.txt --ploidy 1 --proj 87,97,23 -a --prefix three_groups --unfolded
```
## 2. Prepare est and tpl files
xxx.tpl template file defining the demographic model
xxx.est estimation file defining the parameters
tpl:
```
//Parameters for the coalescence simulation program : fsimcoal2.exe
3 samples to simulate :
//Population effective sizes (number of genes)
NPOP1
NPOP2
NPOP3
//Samples sizes and samples age
87
97
23
//Growth rates  : negative growth implies population expansion
0
0
0
//Number of migration matrices : 0 implies no migration between demes
0
//historical event: time, source, sink, migrants, new deme size, new growth rate, migration matrix index
2 historical event
PA 1 2 1 1 0 0
PB 0 2 1 1 0 0
//Number of independent loci [chromosome]
1 0
//Per chromosome: Number of contiguous linkage Block: a block is a set of contiguous loci
1
//per Block:data type, number of loci, per generation recombination and mutation rates and optional parameters
FREQ 1 0 MUTR
```
est:
```
// Search ranges and rules file
// ****************************

[PARAMETERS]
//#isInt? #name   #dist.#min  #max
//all Ns are in number of haploid individuals
1 NPOP1 unif 100 100000000 output bounded
1 NPOP2 unif 100 100000000 output bounded
1 NPOP3 unif 100 100000000 output bounded
1 PA unif  3000 30000 output bounded
1 PB unif  30001 500000 output bounded
0 MUTR unif 4e-8 5e-9 output bounded

[RULES]
[COMPLEX PARAMETERS]
```
## 3. run fastsimcoal2
run each model 100 repetitions with random seeds
```bash
cp models/modelA_three_groups.tpl three_groups.tpl
screen -L ./batch_fastsimcoal2.py modelA
```
```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from multiprocessing import Pool
import subprocess, sys

def run_fsc(input):
    prefix = input[0]
    seed = input[1]
    subprocess.check_call("~/software/fsc27_linux64/fsc27093 -t {0}_three_groups.tpl -e {0}_three_groups.est -d -0 -l 10 -n 100000 -L 40 -s 0 -M -q -r {1}".format(prefix, seed), shell=True)

if __name__ == '__main__':
    L = []
    files = ["three_groups_jointDAFpop1_0.obs", "three_groups_jointDAFpop2_0.obs", "three_groups_jointDAFpop2_1.obs", "three_groups_MSFS.obs", "three_groups.est", "three_groups.tpl"]
    for i in range(100):
        prefix = sys.argv[1] + "_rep" + str(i)
        L.append([prefix, i])
        for file in files:
            subprocess.check_call("ln -s {0} {1}_{0}".format(file, prefix), shell=True)
    pool = Pool(100)      # Create a multiprocessing Pool
    pool.map(run_fsc, L)
```
The simulated parameters, MaxEstLhood and MaxObsLhood can be found in prefix.bestlhoods.

## 4. Model comparison with max likelihood and AIC
***to be continue***
整理结果(ie. modleA)：
```bash
mkdir modelA_bestlhoods
cp modelA_rep*/*_three_groups.bestlhoods modelA_bestlhoods
mkdir modelA_results
mv modelA_rep* modelA_results
tar cjf modelA_bestlhoods.tar.bz2 modelA_bestlhoods
```

```R
library(tidyverse)

model <- list("modelA", "modelB", "modelC", "modelD", "modelE")
df <- data.frame() 
for (m in model){
  for (i in 0:99){
    t <- read.table(paste0(m, "_bestlhoods/", m,"_rep", i, "_three_groups.bestlhoods"), header = T)
    t$model <- m
    df <- rbind(df, t)
  }
}

k <- 6
df %>% group_by(model) %>% summarise(maxL = max(MaxEstLhood), minL = min(MaxEstLhood), averageL = mean(MaxEstLhood), deltaL = mean(MaxObsLhood) - mean(MaxEstLhood), AIC = 2*k - 2*(mean(MaxEstLhood)/log10(exp(1))))

ggplot(df, aes(x = MaxEstLhood, fill = model)) + geom_histogram(binwidth = 30)
```


## 5. Plot best model
```R
library(tidyverse)

t1 <- data.frame(x = c(0,2,17,15.8), y = c(0,0,20,21))
t2 <- data.frame(x = c(30,28,14.2,16), y = c(0,0,18.8,19))
t3 <- data.frame(x = c(12,14,21.6,19.8), y = c(0,0,11,11))

ggplot() + 
  geom_polygon(data = t1, aes(x=x, y=y), fill = "grey") + 
  geom_polygon(data = t2, aes(x=x, y=y), fill = "grey") + 
  geom_polygon(data = t3, aes(x=x, y=y), fill = "grey") + 
  geom_segment(aes(x = 3, xend = 15.5, y = 18.5, yend = 18.5), linetype = 2, size = 0.7) +
  geom_segment(aes(x = 3, xend = 21, y = 10.5, yend = 10.5), linetype = 2, size = 0.7) +
  geom_text(aes(x=1.5, y=18.5, label = "POP1-3"), fontface = "bold") + 
  geom_text(aes(x=1.5, y=10.5, label = "POP2-3"), fontface = "bold") + 
  geom_text(aes(x=1.5, y=10.5, label = "POP2-3"), fontface = "bold") +
  geom_text(aes(x=1.3, y=0.5, label = "NPOP1"), fontface = "bold") + 
  geom_text(aes(x=13.3, y=0.5, label = "NPOP2"), fontface = "bold") + 
  geom_text(aes(x=28.7, y=0.5, label = "NPOP3"), fontface = "bold")  + 
  geom_text(aes(x=1.3, y=-0.5, label = "POP1"), fontface = "bold") + 
  geom_text(aes(x=13.3, y=-0.5, label = "POP2"), fontface = "bold") + 
  geom_text(aes(x=28.7, y=-0.5, label = "POP3"), fontface = "bold") + 
  geom_text(aes(x=26, y=18, label = "Model A"), fontface = "bold", size = 7) + 
  xlab("") + 
  ylab("") + 
  theme(panel.background = element_blank(), panel.grid = element_blank(), axis.line = element_blank(), axis.ticks = element_blank(), axis.text = element_blank())
```