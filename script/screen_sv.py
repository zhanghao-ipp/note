#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

with open("fa_245_pan.vcf", 'r') as f1, open("sv_high_frequency.vcf", "w") as f2:
    f2.write("#CHROM\tPOS\tID\tREF_LEN\tALT_LEN\tAF\tAN\n")
    for line in f1:
        if not line.startswith("#"):
            m = re.search(r"AF=(\S+);AN=(\d+);AT", line)
            af_alt = m.group(1).split(",")
            af = 0
            for i in af_alt:
                if float(i) > af:
                    af = float(i)
            an = int(m.group(2))
            if af >= 0.05 and an >= 200:
                line1 = line.split()
                alt_all = line1[4].split(",")
                alt = 0
                for i in alt_all:
                    if len(i) > alt:
                        alt = len(i)
                if len(line1[3]) != alt:
                    f2.write(line1[0] + "\t" + line1[1] + "\t" + line1[2] + "\t" + str(len(line1[3])) + "\t" + str(alt) + "\t" + str(af) + "\t" + str(an) + "\n")