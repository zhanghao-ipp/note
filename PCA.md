# PCA analysis by smartpca (EIGENSOFT)

## 1. change the chr name first
smartpca just support chromosome name 1-22 and X, Y
```bash
zcat fa_245_gatk_vcfR_noasterisk.vcf.gz | sed 's/Fasi_180197_V1_chr0//g' > fa_245_gatk_vcfR_noasterisk_chrom.vcf
``` 
## 2. generate the input files
generate ".bed .bim .fam"
```bash
plink --vcf fa_245_gatk_vcfR_noasterisk_chrom.vcf --out fa_245 --keep-allele-order --allow-no-sex --set-missing-var-ids @:#\$1\$2 --make-bed
awk '{print $1,$2,$3,$4,$5,1}' fa_245.fam > fa_245_pca.fam
```
edit the parameters file "fa_245.par"
```bash
genotypename: fa_245.bed
snpname: fa_245.bim
indivname: fa_245_pca.fam
snpweightoutname: fa_245.snpeig
evecoutname: fa_245.eigs
evaloutname: fa_245.eval
phylipoutname: fa_245.fst
numoutevec: 20
numoutlieriter: 0
altnormstyle: NO
missingmode: NO
ldregress: 0
noxdata: YES
nomalexhet: YES
``` 
## 3. run smartpca
```bash
docker run --rm -u $(id -u):$(id -u) -v $(pwd):/home -w /home quay.io/biocontainers/eigensoft:8.0.0--h2469040_1 smartpca -p fa_245.par
```
# PCA analysis by PLINK

## 1. format transform
add var id by --set-missing-var-ids
```bash
plink --vcf fa_245_gatk_vcfR_noasterisk_chrom_maf001.vcf.recode.vcf --recode -out fa_245 --set-missing-var-ids @:#\$1\$2 --allow-extra-chr --make-bed
```

## 2. pca analysis
use var-wts to get SNP weights/loadings
```bash
plink --allow-extra-chr --threads 20 -bfile fa_245 --pca 20 'var-wts' --out fa_245
# make the position column
perl -pe "s/Fasi_180197_V1_chr\d+:(\d+)\S+/\1/g" fa_245.eigenvec.var > fa_245_maf001.eigenvec.var.modify
```